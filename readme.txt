Starting the game

1) Set SPACECRAFT_SDK to [depot]/SDK

2) Set correct debugger properties for the SpaceCrafts project

  - Open project properties of SpaceCrafts
    - Goto Configuration Properties > Debugging
      - Select "Debug" Configuration
      - Set Command to: $(OutDir)\$(ProjectName)_d.exe
      - Set working directory to: $(OutDir)
    - Select "Release" Configuration
      - Set Command to: $(OutDir)\$(ProjectName).exe
      - Set working directory to: $(OutDir)

3) Start the game!